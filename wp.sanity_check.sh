#!/usr/bin/env bash
DATA=$RANDOM
CRED='\033[0;31m'
CGREEN='\033[0;32m'
CBLUE='\033[0;34m'
CWHITE='\033[0;37m'
CNC='\033[0m'

cmdAvailable () {
  # check if command is available in $PATH
  # $1 - command to check
  # $2 - suggested package to install (optional)
  type -p "$1" &>/dev/null
  ERRCD=$?
  if [[ $ERRCD -ne 0 ]] ; then
    printf "  $CBLUE$1$CNC is missing"
    if [[ ! -z $2 ]] ; then
      printf " (try installing $CBLUE$2$CNC)"
    fi
    printf "\n"
  fi
  return $ERRCD
}

testEq () {
  # return "passed" if $1 == $2 and "failed" otherwise
  if [[ $1 -eq $2 ]]; then
    printf " $CGREEN passed $CNC\n"
  else
    printf " $CRED failed $CNC\n"
  fi
}

printf "|$CWHITE Wildpipes interactive tests $CNC|\n"


# 01 : terminal <=> clipboard
if true; then
  printf " 01: t2c   &&  c2t                 :"
  DEPSOK=true && \
    cmdAvailable "xclip" || DEPSOK=false
  if $DEPSOK; then
    echo "$DATA" | ./wp.t2c
    TEST=$(./wp.c2t)
    testEq "$DATA" "$TEST"
  fi
fi


# 02 : terminal <=> termbin.com
if true; then
  printf " 02: t2tb  && tb2t                 :"
  DEPSOK=true && \
    cmdAvailable "nc" "gnu-netcat" && \
    cmdAvailable "curl" || DEPSOK=false
  if $DEPSOK; then
    TEST=$(echo "$DATA" | ./wp.t2tb | ./wp.tb2t)
    testEq "$DATA" "$TEST"
  fi
fi


# 03 : terminal => qr code on terminal
#      qr code on display => terminal
if true; then
  printf " 03: t2qrt && qrd2t                :"
  DEPSOK=true && \
    cmdAvailable "maim" && \
    cmdAvailable "zbarimg" "zbar-desktop" && \
    cmdAvailable "qrencode" || DEPSOK=false
  if $DEPSOK; then
    echo -e "\r                                                            \r\e[1A"
    echo "Scan qr with mouse:"
    echo "$DATA" | ./wp.t2qrt
    TEST=$(./wp.qrd2t)
    echo -e "\r\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A\e[K\e[1A"
    printf " 03: t2qrt && qrd2t                :"
    testEq "$DATA" "$TEST"
  fi
fi


# 04 : terminal => qr code in file
#      image => display
#      qr code on display => terminal
if true; then
  printf " 04: t2qrf | f2d && qrd2t          :"
  DEPSOK=true && \
    cmdAvailable "feh" && \
    cmdAvailable "maim" && \
    cmdAvailable "zbarimg" "zbar-desktop" && \
    cmdAvailable "qrencode" || DEPSOK=false
  if $DEPSOK; then
    printf "\r                                    \r"
    echo "Scan qr with mouse:"
    timeout 10 bash -c "echo \"$DATA\" | ./wp.t2qrf | ./wp.f2d" &
    QRW_PID=$!
    TEST=$(./wp.qrd2t)
    echo $TEST
    kill $QRW_PID
    echo -e "\r\e[1A\e[K\r\e[1A\e[K\r\e[1A"
    printf " 04: t2qrf | f2d && qrd2t          :"
    testEq "$DATA" "$TEST"
  fi
fi


# 05 : terminal with text => terminal (text detected with ocr)
if true; then
  printf " 05: d2ocrt                        :"
  DEPSOK=true && \
    cmdAvailable "maim" && \
    cmdAvailable "tesseract" || DEPSOK=false
  if $DEPSOK; then
    DDATA="AMOGUS"
    printf "\r                                    \r"
    echo "-------------------------"
    echo -e "Scan text with mouse:\n"
    echo -e "     $DDATA\n"
    echo "-------------------------"
    TEST=$(./wp.d2ocrt)
    echo -e "\r\e[1A\e[K\r\e[1A\e[K\r\e[1A\e[K\r\e[1A\e[K\r\e[1A\e[K\r\e[1A\e[K\r\e[1A"
    printf " 05: d2ocrt                        :"
    testEq "$DDATA" "$TEST"
  fi
fi



# 06 : terminal => sound message 
#      sound message => sound (with message)
#      sound (with message) => terminal
if true; then
  printf " 06: t2sf  | f2s && sf2t           :"
  # try to copy ggwave-to-file and ggwave-rx to /usr/local/bin or ~/.local/bin or and build/bin to PATH
  DEPSOK=true && \
    cmdAvailable "ggwave-to-file" "https://github.com/ggerganov/ggwave" && \
    cmdAvailable "ggwave-rx" "https://github.com/ggerganov/ggwave" && \
    cmdAvailable "play" "sox" || DEPSOK=false
  if $DEPSOK; then
    printf "\r                                    \r"
    (sleep 1 && echo "$DATA" | ./wp.t2sf | ./wp.f2s) &
    TEST=$(./wp.sf2t)
    printf " 06: t2sf  | f2s && sf2t           :"
    testEq "$DATA" "$TEST"
  fi
fi


# 07 : terminal => telegram bot message
#      message to telegram bot => terminal
if true; then
  printf " 07: t2tg  && tg2t                 :"
  DEPSOK=true && \
    cmdAvailable "curl" && \
    cmdAvailable "jq" || DEPSOK=false
  if $DEPSOK; then
    printf "\r                                    \r"
    echo "$DATA" | ./wp.t2tg
    echo "Forward message from bot to itself and press <Enter>"
    read
    TEST=$(./wp.tg2t)
    echo -e "\e[1A\e[K\e[1A\e[K\e[1A"
    printf " 07: t2tg  && tg2t                 :"
    testEq "$DATA" "$TEST"
  fi
fi

