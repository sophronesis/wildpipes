# wildpipes

Simple bash wrapper with main goal of transferring terminal output or file content to different endpoints (sound, qr codes, camera, clipboard, telegram bot message, etc.) using shell pipes and visa versa.

## Getting started

This project was started as a meme way of sending command output to clipboard and ended up as a way to send your public ssh key over speakers and microphone. 
Here you will find basic script pack that you can use to pipe different sources and outputs effectively. 

## Scripts
Script names are formatted like `wp.X` 
| Script | Usage |
|--|--|
|wp.sanity_check.sh|showcase of some features + dependency tests|
|||
|wp.c2f| X clipboard => file |
|wp.c2t| X clipboard => terminal |
|wp.cam2f| image from webcam => file |
|wp.d2f| image from display => file |
|wp.d2ocrt| image from display => terminal (recognized text from image) |
|wp.f2c| file => X clipboard |
|wp.f2d| (image) file => show image on display|
|wp.f2s| (audio) file => sound |
|wp.f2tg| file => telegram bot |
|wp.qrd2t| image from display with QR code => terminal |
|wp.qrf2t| (image) file with QR code => terminal |
|wp.sf2t| sound message (ggwave) => terminal |
|wp.t2c| terminal => X clipboard  |
|wp.t2ff| terminal => firefox tab |
|wp.t2g|  terminal => google given string |
|wp.t2qrf| terminal => (QR code image) file |
|wp.t2qrt| terminal => QR code as ASCII characters in terminal |
|wp.t2sf| terminal => sound message (ggwave) |
|wp.t2tb| terminal => termbin.com link|
|wp.t2tg| terminal => telegram bot message|
|wp.tb2t| termbin.com link => terminal |
|wp.tg2t| last telegram bot message => terminal|

## Visualisation
![](index.png)

## Installation
<del>just use `setup.sh` lmao</del>
* instal dependencies from `deps`
* add `TGCLIP_RECIEVER_ID`, `TGCLIP_SENDER_NAME` and `TGCLIP_BOT_TOKEN` to shell
* add this folder to path or `wp.X` scripts to `/usr/local/bin` or `~/.local/bin`

## License
MIT license


